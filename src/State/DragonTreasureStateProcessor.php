<?php
declare(strict_types=1);

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\ApiResource\DragonTreasureApi;
use App\Entity\Notification;
use App\Repository\DragonTreasureRepository;
use Doctrine\ORM\EntityManagerInterface;

final class DragonTreasureStateProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly DragonTreasureRepository $dragonTreasureRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly EntityClassDtoStateProcessor $innerProcessor,
    ) {
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        assert($data instanceof DragonTreasureApi);

        $previousData = $context['previous_data'] ?? null;
        if ($previousData instanceof DragonTreasureApi
            && $data->isPublished
            && $previousData->isPublished !== $data->isPublished) {
            $entity = $this->dragonTreasureRepository->find($data->id);
            $notification = new Notification();
            $notification->setDragonTreasure($entity);
            $notification->setMessage('Treasure has been published!');
            $this->entityManager->persist($notification);
            $this->entityManager->flush();
        }

        return $this->innerProcessor->process($data, $operation, $uriVariables, $context);
    }
}
