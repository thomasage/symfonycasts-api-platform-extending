<?php
declare(strict_types=1);

namespace App\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\Pagination\Pagination;
use ApiPlatform\State\Pagination\TraversablePaginator;
use ApiPlatform\State\ProviderInterface;
use App\ApiResource\DailyQuest;
use App\ApiResource\QuestTreasure;
use App\Enum\DailyQuestStatusEnum;
use App\Repository\DragonTreasureRepository;

final class DailyQuestStateProvider implements ProviderInterface
{
    public function __construct(
        private readonly DragonTreasureRepository $dragonTreasureRepository,
        private readonly Pagination $pagination,
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        if ($operation instanceof CollectionOperationInterface) {
            $currentPage = $this->pagination->getPage($context);
            $itemsPerPage = $this->pagination->getLimit($operation, $context);
            $offset = $this->pagination->getOffset($operation, $context);
            $totalItems = $this->countTotalQuests();

            $quests = $this->createQuests($offset, $itemsPerPage);

            return new TraversablePaginator(new \ArrayIterator($quests), $currentPage, $itemsPerPage, $totalItems);
        }

        return $this->createQuests(0, $this->countTotalQuests())[$uriVariables['dayString']] ?? null;
    }

    private function countTotalQuests(): int
    {
        return 50;
    }

    /**
     * @return DailyQuest[]
     */
    private function createQuests(int $offset, int $limit = 50): array
    {
        $treasures = $this->dragonTreasureRepository->findBy([], [], 10);

        $quests = [];
        for ($i = $offset; $i < ($offset + $limit); $i++) {
            $quest = new DailyQuest(new \DateTimeImmutable(sprintf('-%d days', $i)));
            $quest->difficultyLevel = $i % 10;
            $quest->lastUpdate = new \DateTimeImmutable(sprintf('-%d days', rand(10, 100)));
            $quest->questDescription = sprintf('Description %d', $i);
            $quest->questName = sprintf('Quest %d', $i);
            $quest->status = $i % 2 === 0 ? DailyQuestStatusEnum::ACTIVE : DailyQuestStatusEnum::COMPLETED;
            $randomTreasure = $treasures[array_rand($treasures)];
            $quest->treasure = new QuestTreasure(
                $randomTreasure->getName(),
                $randomTreasure->getValue(),
                $randomTreasure->getCoolFactor()
            );
            $quests[$quest->getDayString()] = $quest;
        }

        return $quests;
    }
}
