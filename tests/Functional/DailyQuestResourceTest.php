<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use App\Factory\DragonTreasureFactory;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * @covers(\App\ApiResource\DailyQuest)
 */
final class DailyQuestResourceTest extends ApiTestCase
{
    use Factories, ResetDatabase;

    public function testPatchCanUpdateStatus(): void
    {
        // Quests need at least some treasures to be available
        DragonTreasureFactory::createMany(5);

        $day = new \DateTime('-2 days');
        $this->browser()
            ->patch('/api/quests/'.$day->format('Y-m-d'), [
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                ],
                'json' => [
                    'status' => 'completed',
                ],
            ])
            ->assertStatus(200)
            ->assertJsonMatches('status', 'completed');
    }
}
